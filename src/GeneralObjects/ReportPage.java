/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GeneralObjects;

import static java.awt.Color.BLACK;
import javafx.print.PageOrientation;
import javafx.print.Paper;
import javafx.print.Printer;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

/**
 *
 * @author rongin
 */
public class ReportPage {

    private Canvas canvas_;
    public Paper paper;

    private void initCanvas(Canvas canvas) {
        canvas_ = canvas;
        canvas_.setWidth(paper.getWidth());
        canvas_.setHeight(paper.getHeight());
    }

    public ReportPage(Canvas canvas) {
        paper = Printer.getDefaultPrinter().createPageLayout(Paper.A4, PageOrientation.PORTRAIT, Printer.MarginType.HARDWARE_MINIMUM).getPaper();
        initCanvas(canvas);
    }

    public GraphicsContext getGC() {
        return canvas_.getGraphicsContext2D();
    }

    public ReportPage(Paper p, Canvas canvas) {
        paper = p;
        initCanvas(canvas);
    }

    public double getWidthMM() {
        //case MM    : return (int)( ( (dim * 72) / 25.4) + 0.5);
        return (int) (((paper.getWidth() * 25.4) / 72) + 0.5);
    }

    public double getHeightMM() {
        //case MM    : return (int)( ( (dim * 72) / 25.4) + 0.5);
        return (int) (((paper.getHeight() * 25.4) / 72) + 0.5);
    }

    public void Draw() {
        Paint cur_p = getGC().getFill();
        getGC().setFill(new Color(1, 0, 0, 1));
        getGC().fillRect(0, 0, paper.getWidth(), paper.getHeight());
        getGC().setFill(cur_p);
    }
}
