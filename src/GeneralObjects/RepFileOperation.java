/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GeneralObjects;

import java.io.File;
import java.io.IOException;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.WritableImage;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import jfxMTReportApp.FXMLDocumentController;

/**
 *
 * @author rongin
 */
public class RepFileOperation {
    
    private enum eFile {
        PNG, PROJECT
    }

    private enum eFileOperation {
        SAVE, LOAD
    }
    
    private static FileChooser.ExtensionFilter GetFileExtensionFilter(eFile file_type) {
        switch (file_type) {
            case PNG:
                return new FileChooser.ExtensionFilter("PNG image file (*.png)", "*.png");
            case PROJECT:
                return new FileChooser.ExtensionFilter("Project file (*.prj)", "*.prj");
        }
        return null;
    } 
    private static File GetFileFromDialog(FileChooser fdialog, eFileOperation file_operation){
        if(fdialog==null)
        {
            return null;
        }       
        switch(file_operation)
        {
            case SAVE:
                return fdialog.showSaveDialog(null);
            case LOAD:
                return fdialog.showOpenDialog(null);
        }
        return null;
    }
    private static FileChooser GetFileChooser(eFile file_type){
        FileChooser fdialog = new FileChooser();
        FileChooser.ExtensionFilter filter = GetFileExtensionFilter(file_type);
        fdialog.getExtensionFilters().add(filter);
        return fdialog;
    }
   
    public static void SaveToPng(Canvas canvas) throws IOException {
        if(canvas==null)
        {
            return;
        }
        FileChooser fdialog = GetFileChooser(eFile.PNG);
        File file = GetFileFromDialog(fdialog, eFileOperation.SAVE);
        if (file == null) {
            return;
        }
        WritableImage img = canvas.snapshot(null, null);
        ImageIO.write(SwingFXUtils.fromFXImage(img, null), "png", file);
    }
    public static void SaveProject() throws IOException {
        FileChooser fdialog = GetFileChooser(eFile.PROJECT);
        File file = GetFileFromDialog(fdialog, eFileOperation.SAVE);
        if (file == null) {
            return;
        }
    }
    public static void LoadProject() throws IOException {
        FileChooser fdialog = GetFileChooser(eFile.PROJECT);
        File file = GetFileFromDialog(fdialog, eFileOperation.LOAD);
        if (file == null) {
            return;
        }
    }
}
