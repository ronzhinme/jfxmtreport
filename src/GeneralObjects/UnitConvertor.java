/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GeneralObjects;

import com.sun.javafx.print.Units;

/**
 *
 * @author rongin
 */
public final class UnitConvertor {

    public static double toPoint(double value, Units fromunit) {
        if (fromunit == null) {
            return 0;
        }
        switch (fromunit) {
            case POINT:
                return value;
            case INCH:
                return value * 72;
            case MM:
                return value * (72 / 25.4);
        }
        return value;
    }

    public static double toInch(double value, Units fromunit) {
        if (fromunit == null) {
            return 0;
        }
        switch (fromunit) {
            case POINT:
                return value / 72;
            case INCH:
                return value;
            case MM:
                return value / 25.4;
        }
        return value;
    }

    public static double toMM(double value, Units fromunit) {
        if (fromunit == null) {
            return 0;
        }
        switch (fromunit) {
            case POINT:
                return value * (25.4 / 72);
            case INCH:
                return value * 25.4;
            case MM:
                return value;
        }
        return value;
    }

    public static double getKoef(Units from, Units to) {
        if (from == null || to == null) {
            return 0;
        }

        switch (to) {
            case POINT:
                return toPoint(1, from);
            case INCH:
                return toInch(1, from);
            case MM:
                return toMM(1, from);
        }
        return 1;
    }
}
