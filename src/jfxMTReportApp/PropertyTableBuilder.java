/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jfxMTReportApp;

import javafx.scene.layout.GridPane;
import PropertyGrid.*;

/**
 *
 * @author 1
 */
public class PropertyTableBuilder {

    public static void AddProperty(GridPane pane, PropertyBase prop) {
        pane.add(prop.GetLabel(), 0, pane.getChildren().size() / 2);
        pane.add(prop.GetControl(), 1, (pane.getChildren().size() - 1) / 2);
    }
}
