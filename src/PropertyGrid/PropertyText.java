/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PropertyGrid;

import javafx.scene.Node;
import javafx.scene.control.TextField;

/**
 *
 * @author 1
 */
public class PropertyText extends PropertyBase {

    private final TextField txtval_;

    public PropertyText() {
        this.txtval_ = new TextField();
    }

    public void SetValue(String val) {
        txtval_.setText(val);
    }

    public String GetValue() {
        return txtval_.getText();
    }

    @Override
    public Node GetControl() {
        return txtval_;
    }
}
