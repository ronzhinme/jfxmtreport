/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PropertyGrid;

import javafx.scene.Node;
import javafx.scene.control.Label;

/**
 *
 * @author 1
 */
public abstract class PropertyBase {

    private final Label lblname_ = new Label();

    public void SetName(String name) {
        lblname_.setText(name);
    }

    public String GetName() {
        return lblname_.getText();
    }

    public Label GetLabel() {
        return lblname_;
    }

    public abstract Node GetControl();
}
